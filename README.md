Ansible Role:  Win_Office_2019
=========

Эта роль установит Microsoft Office Professional Plus 2019 на ОС Windows.

Requirements
------------

None.

Role Variables
--------------

Доступные переменные перечислены вместе со значениями по умолчанию (см. `defaults/main.yml`).
Дистрибутив Microsoft Office Professional Plus 2019 ввиду того что имеет большой размер размещен на Веб портале:
  - win_office2019_get_url: http://citov.ekaterinburg.fsin.uis/doki/soft/Microsoft_Office/Microsoft_Office2019_ProfessionalPlus/

Настройки тихой установки находятся в файле AUTORUN.exe -> ИНФОРМАЦИЯ О СБОРКЕ

Dependencies
------------

None.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - win_office2019

License
-------

BSD

Author Information
------------------

Chubik Sergey, sergey.chubik@mail.ru.
Chubik Sergey, chubik@ekaterinburg.fsin.uis.
